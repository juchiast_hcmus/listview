package com.example.qwe.my_question;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

class MonAnAdapter extends ArrayAdapter<Question> {
    private final Context context;

    MonAnAdapter(Context mainActivity, ArrayList<Question> questions) {
        super(mainActivity, 0, questions);
        this.context = mainActivity;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = createItemView(position);
        }
        return convertView;
    }

    private View createItemView(int position) {
        View view = createViewByLayoutID(R.layout.layout_item);
        Question itemData = getItem(position);
        if (itemData != null) {
            itemData.bindData(view, context);
        }
        return view;
    }

    private View createViewByLayoutID(int layout_item) {
        return LayoutInflater.from(context).inflate(layout_item, null);
    }
}
