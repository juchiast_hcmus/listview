package com.example.qwe.my_question;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public interface Question {
    void bindData(View view, Context context);
}

class SingleChoice implements Question {

    @Override
    public void bindData(View view, Context context) {
        TextView q = view.findViewById(R.id.question);
        q.setText("2x - 1 = 0 ?");
        RadioGroup g = new RadioGroup(context);
        for (String text : new String[]{"-0.5", "0.5", "1.5", "2.5"}) {
            g.addView(getRadioButton(context, text));
        }
        LinearLayout ans = view.findViewById(R.id.choice);
        ans.addView(g);
    }

    @NonNull
    private RadioButton getRadioButton(Context context, String text) {
        RadioButton c1 = new RadioButton(context);
        c1.setText(text);
        return c1;
    }
}

class MultipleChoice implements Question {

    @Override
    public void bindData(View view, Context context) {
        TextView q = view.findViewById(R.id.question);
        q.setText("2x² + 3x + 1 = 0 ?");

        LinearLayout ans = view.findViewById(R.id.choice);
        for (String text : new String[]{"-1", "3", "0.5", "-0.5"}) {
            ans.addView(getCheckBox(context, text));
        }
    }

    @NonNull
    private CheckBox getCheckBox(Context context, String text) {
        CheckBox b = new CheckBox(context);
        b.setText(text);
        return b;
    }
}

class Fill implements Question {

    @Override
    public void bindData(View view, Context context) {
        TextView q = view.findViewById(R.id.question);
        q.setText("Tại sao??");

        LinearLayout ans = view.findViewById(R.id.choice);
        ans.addView(new EditText(context));
    }
}