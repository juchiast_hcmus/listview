package com.example.qwe.my_question;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fillQuestions();
    }

    private void fillQuestions() {
        ArrayList<Question> questions = initArrayList();
        MonAnAdapter adapter = new MonAnAdapter(this, questions);
        ListView l = findViewById(R.id.view);
        l.setAdapter(adapter);
    }

    private ArrayList<Question> initArrayList() {
        ArrayList<Question> questions = new ArrayList<>();
        questions.add(new SingleChoice());
        questions.add(new MultipleChoice());
        questions.add(new Fill());
        return questions;
    }
}